define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "entryscape-commons/defaults",
    "entryscape-commons/dialog/TitleDialog",
    "dojo/text!./HarvestDetailsDialogTemplate.html",
    "dijit/_WidgetsInTemplateMixin",
    "di18n/NLSMixin",
    "dojo/_base/array",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/dom-construct"
], function (declare, lang, defaults, TitleDialog, template,_WidgetsInTemplateMixin,NLSMixin,array,domAttr,domStyle,domConstruct) {

    defaults.get("namespaces").add("storepr", "http://entrystore.org/terms/pipelineresult#");

    return declare([TitleDialog.Content, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
        templateString: template,
        nlsBundles: ["psidatakollen"],

        maxWidth: 800,
        nlsHeaderTitle: "apiInfoDialogHeader",
        nlsFooterButtonLabel: "apiInfoDialogCloseLabel",
        nlsFooterButtonTitle :  "apiInfoDialogfooterButtonTitle",
        includeFooter : false,

        localeChange: function() {
            this.dialog.updateLocaleStrings(this.NLSBundles.dataset);
        },
        open: function(params) {
            this.entry = params.row.entry;

        }
    });
});

