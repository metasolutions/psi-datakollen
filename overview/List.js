define([
    "dojo/_base/declare",
    "store/solr",
    "store/types",
    "store/terms",
    "entryscape-commons/defaults",
    "./HarvestingRow",
    "entryscape-commons/list/common/BaseList"
], function (declare, solr, types, terms, defaults, HarvestingRow, BaseList) {

    defaults.get("namespaces").add("storepr", "http://entrystore.org/terms/pipelineresult#");

    return declare([BaseList], {
        nlsBundles: ["list", "psidatakollen"],
        includeCreateButton: false,
        includeEditButton: false,
        includeInfoButton: false,
        includeHead: false,
        searchVisibleFromStart: true,
        tags: [],
        tagsModifier: null,
        succeeded: null,
      //  rowClickDialog: "openHarvestingDetails",

        rowClass: HarvestingRow,

        postCreate: function() {
            this.inherited("postCreate", arguments);
        },

        updateLocaleStrings: function(generic, specific) {
            this.inherited(arguments);
            var b = this.NLSBundles.psidatakollen;
            this.getView().setTableHead("<tr class='psirow'>" +
                "<th class='vmiddle entryName'>"+ b.organizationLabel +"</th>" +
                "<th class='vmiddle psiname'>"+ b.psiPageLabel +"</th>" +
                "<th class='vmiddle dcatAP'>"+ b.dcatAPLabel +"</th>" +
                "<th class='vmiddle harvestDate'>"+ b.checkedLabel +"</th>" +
                "</tr>");
        },

        showStopSign: function() {
            return false;
        },
            /*        getTemplate: function() {
                        if (!this.template) {
                            this.template = defaults.get("itemstore").createTemplateFromChildren([
                                "dcterms:title",
                                "dcterms:description"
                            ]);
                        }
                        return this.template;
                    },*/
        getSearchObject: function() {
            //return solr.graphType("http://www.w3.org/ns/dcat#Catalog");
            var sobj = solr.graphType(types.GT_PIPELINERESULT).tagLiteral(this.tags, this.tagsModifier);
            if (this.succeeded === true) {
                return sobj.status(terms.status.Succeeded);
            } else if (this.succeeded === false) {
                return sobj.status(terms.status.Failed);
            }
            return sobj;
        }
    });
});