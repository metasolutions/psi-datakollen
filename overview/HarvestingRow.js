define([
    "dojo/_base/declare",
    "dojo/dom-attr",
    "entryscape-commons/list/EntryRow",
    "dojo/string",
    "dojo/date/locale",
    "entryscape-commons/defaults",
    "dojo/text!./HarvestingRowTemplate.html"
], function (declare, domAttr, EntryRow, string, locale, defaults, template) {

    var checkStr = "<i data-dojo-attach-point='statusNode' class='fa fa-check fa-lg'></i>";

    return declare([EntryRow], {
        templateString: template,
        entry: null,
        showCol1: true,
        showCol3: true,
        //true forces menu, false disallows menu, otherwise nr. of buttons will decide (> 1 -> menu)
        rowButtonMenu: null,

        updateLocaleStrings: function (generic, specific) {
            this.inherited(arguments);
        },
        render: function() {
            this.inherited(arguments);
            var md = this.entry.getMetadata();
            if (md.findFirstValue(null, "storepr:psidata")) {
                domAttr.set(this.psiPage, "innerHTML", checkStr);
            }
            if (md.findFirstValue(null, "storepr:dcatMerge")) {
                domAttr.set(this.dcatAP, "innerHTML", checkStr);
            }
        },

        renderDate: function () {
            if (this.nlsGenericBundle) { //Localization strings are loaded.
                try {
                    var cDate = this.entry.getEntryInfo().getCreationDate();
                    var cDateMedium = locale.format(cDate, {selector: "date", formatLength: "medium"});
                    var cDateFull = locale.format(cDate, {formatLength: "medium"});
                    var tStr = string.substitute(this.nlsSpecificBundle.creationDateTitle, {date: cDateFull});
                    domAttr.set(this.harvestDate, {"innerHTML": cDateMedium, "title": tStr});
                } catch (e) {
                    //TODO strange case when gregorian has not been loaded in time.
                }
            }
        }
    });
});