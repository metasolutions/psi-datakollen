define({
    root: {
        creationDateTitle: "Checked source at ${date}",
        listSearchPlaceholder: "Search for (minimum three characters) ...",
        organizationLabel: "Municipality/administrative authority",
        psiPageLabel: "PSI page",
        dcatAPLabel: "DCAT-AP",
        checkedLabel: "Checked"
    },
    sv: true
});