define({
    creationDateTitle: "Kollade källan den ${date}",
    listSearchPlaceholder: "Sök efter (minst tre bokstäver) ...",
    organizationLabel: "Kommun/myndighet",
    psiPageLabel: "PSI-sida",
    dcatAPLabel: "DCAT-AP",
    checkedLabel: "Kontrollerad",
});