define(["entryscape-commons/merge", "config/local"], function(merge, local) {
        return merge({
            theme: {
                appName: "Sandbox",
                showModuleNameInHeader: true
            },
            locale: {
                fallback: "en",
                supported: [
                    {lang: "en", flag: "gb", label: "English", labelEn: "English"},
                    {lang: "sv", flag: "se", label: "Svenska", labelEn: "Swedish"}
                ]
            },
            entrystore: {
                repository: "http://localhost:8080/store/" //CHANGE THIS LINE (to reflect the correct URL to your EntryStore installation)
            },
            itemstore: {
                bundles: [
                    "./libs/rdforms/templates/dcterms.json",
                    "./libs/rdforms/templates/foaf.json",
                    "./libs/rdforms/templates/skos.json",
                    "./libs/rdforms/templates/vcard.json"
                ]
            },
            site: {
                siteClass: "spa/Site",
                controlClass: "entryscape-commons/nav/Layout",
                startView: "psiapproved",
                modules: [
                    {
                        name: "psidatakollen",
                        title: {en: "PSI-datakollen", sv: "PSI-datakollen"},
                        tooltip: {en: ""},
                        faClass: "sitemap",
                        views: ["psiapproved", "psinotapproved", "psiwhat", "psiabout"]
                    }
                ],
                views: [
                    {
                        "name": "start", "class": "entryscape-commons/nav/Start",
                        "title": {en: "Start", sv: "Start"}
                    },
                    {
                        "name": "psiapproved", "class": "psidatakollen/overview/List",
                        "title": {en: "Approved", sv: "Godkända"},
                        "constructorParams": {tags: ["latest", "psi"], succeeded: true}
                    },
                    {
                        "name": "psinotapproved", "class": "psidatakollen/overview/List",
                        "title": {en: "Not approved", sv: "Ej Godkända"},
                        "constructorParams": {tags: ["latest", "psi"], succeeded: false}
                    },
                    {
                        "name": "psiwhat", "class": "entryscape-commons/text/TextView",
                        "title": {en: "What is the PSI-directive", sv: "Vad är PSI direktivet"},
                        "constructorParams": {path: "psidatakollen/texts/what"}
                    },
                    {
                        "name": "psiabout", "class": "entryscape-commons/text/TextView",
                        "title": {en: "About PSI-datakollen", sv: "Om PSI-datakollen"},
                        "constructorParams": {path: "psidatakollen/texts/about"}
                    }
              ]
            }
        }, local);
    }
);